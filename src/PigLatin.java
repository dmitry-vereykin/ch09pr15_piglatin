/**
 * Created by Dmitry Vereykin on 7/22/2015.
 */
import java.util.Scanner;

public class PigLatin {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String input;
        System.out.print("Enter a string: ");
        input = keyboard.nextLine();

        System.out.print("English: " + input.toUpperCase());

        String[] sepStrings = separate(input);
        String[] sepStrNew = new String[sepStrings.length];

        System.out.print("\nPig Latin: ");
        for (int n = 0; n < sepStrings.length; n++) {
            sepStrNew[n] = sepStrings[n].substring(1) + sepStrings[n].substring(0, 1);
            sepStrNew[n] += "AY";
            System.out.print(sepStrNew[n].toUpperCase() + " ");
        }
        keyboard.close();
    }

    private static String[] separate(String str) {
        String[] result = str.split("\\s+");
        return result;
    }
}
